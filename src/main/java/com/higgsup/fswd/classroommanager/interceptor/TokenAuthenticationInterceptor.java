package com.higgsup.fswd.classroommanager.interceptor;

import com.higgsup.fswd.classroommanager.controller.stereotype.NoAuthentication;
import com.higgsup.fswd.classroommanager.controller.stereotype.RequiredRoles;
import com.higgsup.fswd.classroommanager.exception.InterceptorException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lent on 4/20/2016.
 */
@Component
public class TokenAuthenticationInterceptor extends HandlerInterceptorAdapter {

    private Map<String, String> sampleTokenRoleMapping = new HashMap<String, String>();

    public TokenAuthenticationInterceptor() {
        sampleTokenRoleMapping.put("0001", "Registered");
        sampleTokenRoleMapping.put("0002", "Administrator");
        // Phải lấy các thông tin này từ database, đây là dữ liệu mẫu fixed
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("auth-token");
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            if (handlerMethod.getMethodAnnotation(NoAuthentication.class) != null) {
                // No required authentication
                return true; // by pass
            } else if (token == null) {
                throw new InterceptorException("Required token", HttpStatus.UNAUTHORIZED); // By default required token
            }

            String role = sampleTokenRoleMapping.get(token);
            RequiredRoles requiredRoles = handlerMethod.getMethodAnnotation(RequiredRoles.class);
            if (requiredRoles != null) {
                for (String requiredRole : requiredRoles.value()) {
                    if (requiredRole.equals(role)) {
                        return true;
                    }
                }
                throw new InterceptorException("No matching role found", HttpStatus.UNAUTHORIZED);
            }
        }
        return true;
    }
}
