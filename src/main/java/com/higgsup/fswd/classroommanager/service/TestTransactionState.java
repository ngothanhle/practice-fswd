package com.higgsup.fswd.classroommanager.service;

import com.higgsup.fswd.classroommanager.model.ClassRoom;
import com.higgsup.fswd.classroommanager.repository.ClassRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by lent on 5/1/2016.
 */
@Service
public class TestTransactionState {
    @Autowired
    private ClassRoomRepository classRoomRepository;

    @Transactional
    public String testState() {
        ClassRoom c1 = new ClassRoom();
        c1.setClassName("New class 01");
        c1.setLocation("Somewhere");
        c1 = classRoomRepository.save(c1);

        c1 = new ClassRoom();
        c1.setClassName("Short");
        c1.setLocation("Else");
        c1 = classRoomRepository.save(c1);

        ClassRoom c1f = classRoomRepository.findOne(c1.getClassName());
        List<ClassRoom> c1l = classRoomRepository.findByClassNameLongerThan5();
        return c1f.getClassName();
    }
}
