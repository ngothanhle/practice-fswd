package com.higgsup.fswd.classroommanager.service;

import com.higgsup.fswd.classroommanager.model.ClassRoom;
import com.higgsup.fswd.classroommanager.model.Student;
import com.higgsup.fswd.classroommanager.repository.ClassRoomRepository;
import com.higgsup.fswd.classroommanager.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassRoomManagerService {
    @Autowired
    private ClassRoomRepository classRoomRepository;
    @Autowired
    private StudentRepository studentRepository;

    public List<ClassRoom> getClassRooms() {
        List<ClassRoom> allClassRooms = (List<ClassRoom>) classRoomRepository.findAll();
        // Hypermedia links building
        for (ClassRoom classRoom : allClassRooms) {
            classRoom.getLinks().put("self", "http://localhost:8080/classes/" + classRoom.getClassName());
            classRoom.getLinks().put("parent", "http://localhost:8080/classes");
            classRoom.getLinks().put("children", "http://localhost:8080/classes/" + classRoom.getClassName() + "/students");
            // TODO: Có cách nào lấy đường dẫn http://localhost:8080/ ko ?
        }
        return allClassRooms;
    }

    public ClassRoom createClassRoom(ClassRoom classRoom) {
        return classRoomRepository.save(classRoom);
    }

    public ClassRoom findClassRoom(String className) {
        return classRoomRepository.findOne(className);
    }

    public Student findStudent(String studentName) {
        return studentRepository.findOne(studentName);
    }

    public Student createStudent(String classRoom, Student student) {
        ClassRoom classRoomObj = findClassRoom(classRoom);
        classRoomObj.getStudents().add(student);
        Student createdStudent = studentRepository.save(student);
        return createdStudent;
    }
}
