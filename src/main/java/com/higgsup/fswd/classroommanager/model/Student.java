package com.higgsup.fswd.classroommanager.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student extends HypermediaLinks {
    @Id
    private String studentName;
    private Integer age;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
