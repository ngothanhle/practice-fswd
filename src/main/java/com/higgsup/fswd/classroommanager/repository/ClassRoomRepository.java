package com.higgsup.fswd.classroommanager.repository;

import com.higgsup.fswd.classroommanager.model.ClassRoom;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassRoomRepository extends CrudRepository<ClassRoom, String> {
    @Query("SELECT c FROM ClassRoom c WHERE LENGTH(c.className) >= 5")
    List<ClassRoom> findByClassNameLongerThan5();
}
