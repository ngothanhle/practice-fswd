package com.higgsup.fswd.classroommanager.controller;

import com.higgsup.fswd.classroommanager.controller.stereotype.NoAuthentication;
import com.higgsup.fswd.classroommanager.model.Student;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by lent on 5/4/2016.
 */
@Controller
public class MultiReturnTypeController {
    @NoAuthentication
    @RequestMapping(value = "/helloData", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Student helloStudentJSON() {
        Student student = new Student();
        student.setStudentName("Great Student");
        return student;
    }

    @NoAuthentication
    @RequestMapping(value = "/helloData", produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public Student helloStudentXML() {
        Student student = new Student();
        student.setStudentName("Great Student");
        return student;
    }
}
