package com.higgsup.fswd.classroommanager.controller;

import com.higgsup.fswd.classroommanager.model.ClassRoom;
import com.higgsup.fswd.classroommanager.model.Student;
import com.higgsup.fswd.classroommanager.service.ClassRoomManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClassRoomController {
    @Autowired
    private ClassRoomManagerService classRoomManagerService;

    @RequestMapping(value = "/classes", method = RequestMethod.GET)
    public List<ClassRoom> classes() {
        return classRoomManagerService.getClassRooms();
    }

    @RequestMapping(value = "/classes", method = RequestMethod.POST)
    public ClassRoom createClass(@RequestBody ClassRoom classRoom) {
        return classRoomManagerService.createClassRoom(classRoom);
    }

    @RequestMapping(value = "/classes/{className}", method = RequestMethod.GET)
    public ClassRoom createClass(@PathVariable("className") String className) {
        return classRoomManagerService.findClassRoom(className);
    }

    @RequestMapping(value = "/classes/{className}/students", method = RequestMethod.GET)
    public List<Student> students(@PathVariable("className") String className) {
        return classRoomManagerService.findClassRoom(className).getStudents();
    }

    @RequestMapping(value = "/classes/{className}/students", method = RequestMethod.POST)
    public Student createStudent(@PathVariable("className") String className, @RequestBody Student student) {
        return classRoomManagerService.createStudent(className, student);
    }

    @RequestMapping(value = "/classes/{className}/students/{studentName}", method = RequestMethod.GET)
    public Student student(@PathVariable("className") String className, @PathVariable("studentName") String studentName) {
        return classRoomManagerService.findStudent(studentName);
    }
}
